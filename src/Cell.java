public class Cell {
    private int x;
    private int y;
    private Cell aboveCell;
    private Cell bottomCell;
    private Cell leftCell;
    private Cell rightCell;
    private boolean isAlive;
    private boolean shouldDie;

    public Cell(int x, int y, boolean isAlive) {
        this.x = x;
        this.y = y;
        this.isAlive = isAlive;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Cell getAboveCell() {
        return aboveCell;
    }

    public void setAboveCell(Cell aboveCell) {
        this.aboveCell = aboveCell;
    }

    public Cell getBottomCell() {
        return bottomCell;
    }

    public void setBottomCell(Cell bottomCell) {
        this.bottomCell = bottomCell;
    }

    public Cell getLeftCell() {
        return leftCell;
    }

    public void setLeftCell(Cell leftCell) {
        this.leftCell = leftCell;
    }

    public Cell getRightCell() {
        return rightCell;
    }

    public void setRightCell(Cell rightCell) {
        this.rightCell = rightCell;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public boolean isShouldDie() {
        return shouldDie;
    }

    public void setShouldDie(boolean shouldDie) {
        this.shouldDie = shouldDie;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "x=" + x +
                ", y=" + y +
                ", aboveCell=" + aboveCell +
                ", bottomCell=" + bottomCell +
                ", leftCell=" + leftCell +
                ", rightCell=" + rightCell +
                ", isAlive=" + isAlive +
                ", shouldDie=" + shouldDie +
                '}';
    }
}