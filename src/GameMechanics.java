import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameMechanics {
    private List<Cell> allCells = new ArrayList<>();

    protected void takeInput(int[][] input){
        for (int i = 0; i < input.length; i++){
            for (int j = 0; j < input[i].length; j++){
                if (input[i][j] == 1){
                    allCells.add(new Cell(i,j,true));
                } else {
                    allCells.add(new Cell(i,j,false));
                }
            }
        }
    }

    protected void doTurn(){
        for (Cell cell : allCells){
            createNeighbours(cell);
            willCellLiveOrDie(cell);
        }
    }

    private boolean willCellLiveOrDie(Cell cell){
        List<Cell> neighbourCells = new ArrayList<>(Arrays.asList(cell.getAboveCell(),cell.getBottomCell(),
                cell.getLeftCell(),cell.getRightCell()));
        int aliveNeighbours = 0;
        for (Cell c : neighbourCells){
            if (c.isAlive()){
                aliveNeighbours++;
            }
        }
        if (cell.isAlive()){
            if (aliveNeighbours == 0 || aliveNeighbours == 1 || aliveNeighbours == 4) {
                cell.setAlive(false);
            }
        } else {
            if (aliveNeighbours==3){
                cell.setAlive(true);
            }
        }
        return cell.isAlive();
    }

    private void createNeighbours(Cell cell){
        if(cell.getAboveCell() == null) {
            allCells.add(new Cell(cell.getX(),cell.getY() + 1,true));
        }
        if(cell.getBottomCell() == null) {
            allCells.add(new Cell(cell.getX(),cell.getY() - 1,true));
        }
        if(cell.getLeftCell() == null) {
            allCells.add(new Cell(cell.getX() - 1,cell.getY(),true));
        }
        if(cell.getRightCell() == null) {
            allCells.add(new Cell(cell.getX() + 1,cell.getY(),true));
        }
    }

    private Cell getCell(int x, int y){
        for (Cell cell : allCells){
            if (cell.getX() == x && cell.getY() == y){
                return cell;
            }
        }
        return null;
    }

    protected void print(){
        int maxX = allCells.get(0).getX();
        int maxY = allCells.get(0).getX();
        for (Cell cell : allCells){
            if (cell.getX() > maxX){
                maxX = cell.getX();
            }
            if (cell.getY() > maxY){
                maxY = cell.getY();
            }
        }
        int[][] printable = new int[maxX][maxY];
        for (int i = 0; i < printable.length; i++){
            for (int j = 0; j < printable[i].length; j++){
                if (getCell(i,j) != null){
                    if (getCell(i,j).isAlive()){
                        printable[i][j] = 1;
                    } else {
                        printable[i][j] = 0;
                    }
                } else {
                    printable[i][j] = 0;
                }
            }
        }
    }
}